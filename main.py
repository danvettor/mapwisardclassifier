from data_treatment import *
from PyWANN.WiSARD import WiSARD
from PIL import Image

		
image_train = ImageData('remote_sense.jpeg')
image_processor = ImageTreatment()
train_data = []
train_labels = []
try:
	# TRATAMENTO DE DADOS CASO EXISTAM
	print "Searching train data..."
	train_data, train_labels = image_train.read_data_file("data/train")
	print "Data treatment..."
	train_data = image_processor.threshold_binarization(train_data, 150)
except Exception as e:
	print "===================="
	print "Caught an exception"
	print "===================="
	print e
	print "Creating image data"
	image_train.create_train_data(10)
	exit(-1)
	
# TREINAMENTO
net = WiSARD(4, bleaching=False)
print "Training WiSARD..."
training_retina = image_processor.resize_image_array(train_data)
net.fit(training_retina, train_labels)

# PREPARACAO DOS DADOS LIDOS PARA SEREM PREDICTED
print "Treating validation image..."
image_validation = ImageData("test_image.jpeg")
validation_data = image_validation.create_validation_data()
validation_retina = image_processor.threshold_binarization(validation_data,100)
validation_retina = image_processor.resize_image_array(validation_retina)

print "Classifying image..."
result_labels = net.predict(validation_retina)


print "Colorizing and saving output image..."
# COLORACAO DO MAPA BASEADO NO RESULTADO
image_processor.colorize_image("test_image.jpeg", result_labels, 100)

print "Done!"

