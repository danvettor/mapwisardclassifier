from PIL import Image
import numpy as np
import cv2
import matplotlib.pyplot as plt
import pickle


class ImageData(object):

	def __init__(self, image_filename):
		self.image = cv2.imread(image_filename,0)
		self.image_data = []

	def read_data_file(self, image_data_filename):
		image_data_file= open(image_data_filename,'r')
		data = pickle.load(image_data_file)
		self.image_data, labels = data
		return self.image_data, labels

	def create_train_data(self, sample_number):
		labels = []
		image_size = self.image.shape[0]
		print type(self.image_data)
		for i in xrange(sample_number):
			random = np.random.randint(0, image_size-100)
			x = random
			y = random
			cropped_image = self.image[x:x+100, y:y+100]
			self.image_data.append(cropped_image)
			#cidade = 1
			#desmatado = 2
			#florestinha = 0
			cv2.imshow('teste', cropped_image)
			cv2.waitKey(0)
		self.save_data()

	def create_validation_data(self, crop_size=100):
		
		image_size = self.image.shape[0]

		iterator = self.image.shape[0]/crop_size
		for i in xrange(iterator):
			x = 100 * i				
			for j in xrange(iterator):
				y = crop_size*j
				cropped_image = self.image[x:x+crop_size, y:y+crop_size]
				self.image_data.append(cropped_image)
		return self.image_data

	def save_data(self):
		save_image = cv2.imread('disquete.jpg',0)
		cv2.imshow('SALVE!', save_image)
		cv2.waitKey(0)
		
		label_file = open("data/labels")
		labels = []
		for line in label_file:
			labels.append(int(line))

		data = (self.image_data, labels)
		saved_file = open("data/train","w")
		pickle.dump(data, saved_file)



class ImageTreatment:

	def threshold_binarization(self, array, threshold=100):
		binarized_array = array
		for i in range(len(array)):
			for j in range(len(array[0])):
				for k in range(len(array[0][0])):
					if array[i][j][k] > threshold:
						binarized_array[i][j][k] = 1
					else:
						binarized_array[i][j][k] = 0
		return binarized_array

	def resize_image_array(self, array):
		one_d_array = []
		for line in array:
			one_d_array.append(line.flatten())
		return np.array(one_d_array)

	def colorize_image(self, image_filename, labels, square_size=100):
		blue_mask = Image.open("colors/blue.png")
		red_mask = Image.open("colors/red.png")
		green_mask = Image.open("colors/green.png")

		labels_index = 0
		image = cv2.imread(image_filename,0)
		squares_number = image.shape[0]/square_size
		
		output_image = Image.open(image_filename)

		for i in xrange(squares_number):
			x = 100 * i
			for j in xrange(squares_number):
				y = 100*j
				if(labels[labels_index] == 0):
					output_image.paste(blue_mask, (x, y), blue_mask)
				elif(labels[labels_index] == 1):
					output_image.paste(red_mask, (x, y), red_mask)
				elif(labels[labels_index] == 2):
					output_image.paste(green_mask, (x, y), green_mask)
				
				labels_index = labels_index + 1
				#cidade = 1
				#desmatado = 2
				#florestinha = 0

		# output_image.show()
		output_image.save("output/result.png")







